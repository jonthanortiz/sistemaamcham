
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
## Mesa
## Acta
class ModeloBase(models.Model):
    created_at =    models.DateTimeField(default=datetime.now(), blank=True)
    user_created =  models.ForeignKey(User,on_delete=models.CASCADE, related_name='%(class)s_requests_user_created')
    update_at =     models.DateTimeField(default=datetime.now(), blank=True)
    user_update =   models.ForeignKey(User,on_delete=models.CASCADE, related_name='%(class)s_requests_user_update')

    class Meta:
        abstract = True

class TipoCompania(ModeloBase):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

class TipoMembresia(ModeloBase):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

class TipoComercio(ModeloBase):
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre

class Socio(ModeloBase):

    STATUS_CHOICES = [
    ('A', 'APROBADO'),
    ('P', 'PENDIENTE'),
]

    FORME_CHOICES = [
    ('SI', 'SOCIENDAD ILIMITADA'),
    ('SA', 'SOCIEDAD ANONIMA'),
]

    SERVICIOS_CHOICES = [
    ('S', 'SERVICIOS'),
    ('P', 'PRODUCTOS'),
    ('SP', 'SERVICIOS Y PRODUCTOS'),
]

    COMPANIA_CHOICES = [
    ('1', 'TIPO 1'),
    ('2', 'TIPO 2'),
    ('3', 'TIPO 3'),
]
    MEMBRESIA_CHOICES = [
    ('1', 'MEMBRESIA 1'),
    ('2', 'MEMBRESIA 2'),
    ('3', 'MEMBRESIA 3'),
]
    COMERCIOS_CHOICES = [
    ('1', 'COMERCIOS 1'),
    ('2', 'COMERCIOS 2'),
    ('3', 'COMERCIOS 3'),
]
    codigo = models.CharField(verbose_name='Código de Socio', max_length=150)
    estado = models.CharField(
        max_length=15,
        choices=STATUS_CHOICES,
    )
    nombre_empresa = models.CharField(max_length=150)
    empresa_telefono = models.CharField(max_length=150)
    empresa_rtn = models.CharField(verbose_name='Codigo RTN de la Empresa',max_length=150)
    empresa_pagina = models.CharField(max_length=150)
    empresa_forma = models.CharField(
        max_length=15,
        choices=FORME_CHOICES,
    )
    empresa_fax = models.CharField(max_length=150)
    empresa_pobox = models.CharField(max_length=150)

    #uBICACIÓN DEL SOCIO
    direccion = models.CharField(max_length=150)
    email = models.CharField(max_length=150)
    telefono = models.CharField(max_length=150)

    #CATEGORIZACIÓN DEL SOCIO
    tipo_compania = models.CharField(
        max_length=15,
        choices=COMPANIA_CHOICES,
    )
    tipo_membresia = models.CharField(
        max_length=15,
        choices=MEMBRESIA_CHOICES,
    )
    tipo_comercio = models.CharField(
        max_length=15,
        choices=COMERCIOS_CHOICES,
    )
    #tipo_compania = models.ForeignKey(TipoCompania, on_delete=models.CASCADE)
    #tipo_membresia = models.ForeignKey(TipoMembresia, on_delete=models.CASCADE)
    #tipo_comercio = models.ForeignKey(TipoComercio, on_delete=models.CASCADE)
    capacidad_produccion_mensual = models.CharField(max_length=150)
    pib = models.CharField(max_length=150)
    numero_empleados = models.CharField(max_length=150)
    empresa_servicios = models.CharField(max_length=150)
    empresa_servicios_ofrecidos =  models.CharField(
        max_length=15,
        choices=SERVICIOS_CHOICES,
    )
    mercado_actual = models.CharField(max_length=150)
    mercado_deseado = models.CharField(max_length=150)
    mercado_interes = models.CharField(max_length=150)

    def __str__(self):
        return self.codigo

class Tipo(ModeloBase):
    nombre = models.CharField(max_length=150)
    def __str__(self):
        return self.nombre

class Requisitos(ModeloBase):
    socio = models.ForeignKey(Socio, on_delete=models.CASCADE)
    escritura_constitucion = models.CharField(max_length=150)
    documento_escritura = models.FileField(upload_to='media/socios/documentos')
    escritura_copiartn = models.CharField(max_length=150)
    documento_copiartn = models.FileField(upload_to='media/socios/documentos')
    certificado_consejoadmon = models.CharField(max_length=150)
    documento_certificado_consejoadmon = models.FileField(upload_to='media/socios/documentos')
    referencia_empresa = models.CharField(max_length=150)
    documento_referencia_empresa = models.FileField(upload_to='media/socios/documentos')
    anio_constitucion = models.CharField(max_length=150)
    documento_anio_constitucion = models.FileField(upload_to='media/socios/documentos')
    formulario_membresia = models.CharField(max_length=150)
    documento_formulario_membresia = models.FileField(upload_to='media/socios/documentos')
    pago_cuota = models.CharField(max_length=150)
    documento_pago_cuota = models.FileField(upload_to='media/socios/documentos')

class Ejecutivo(ModeloBase):
    socio = models.ForeignKey(Socio, on_delete=models.CASCADE)
    tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=150)
    email = models.CharField(max_length=150)
    telefono = models.CharField(max_length=150)
